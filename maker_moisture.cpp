/*
 * Library: maker_moisture.h
 *
 * Organization: MakerRobotics
 * Autors: Maksim Jovovic
 *
 * Date: 27.11.2017.
 * Test: Arduino UNO
 *
 * Note:
 *
 */

#include "Arduino.h"
#include "maker_moisture.h"
#include "stdint.h"

/* Reads the sensor value from the analog pin */
double maker_getMoistureValue(uint8_t analogPin)
{
    return analogRead(analogPin);
}

/* Calculates the average value read from the analog pin */
double maker_getAverageMoistureValue(uint8_t analogPin, uint8_t numTests)
{
    double value;

    for(int i = 0; i < numTests; i++)
    {
        value += maker_getMoistureValue(analogPin);
    }

    value /= numTests;
    return value;
}
