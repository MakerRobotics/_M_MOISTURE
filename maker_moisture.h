#ifndef _MAKER_MOISTURE_
#define _MAKER_MOISTURE_

/**
    Note: Reads the sensor value from the analog pin.

    @param
      analogPin is the pin from which to read the value.

    @return
      the value read from the analogPin.
*/
double maker_getMoistureValue(uint8_t analogPin);

/**
    Note: Calculates the average value read from the sensors analog pin.

    @param
      analogPin is the pin from which to read the value.
      numTests is the number of times the value should be read from the pin. If omitted it will be set to 10 by default.

    @return
      the average value read from the analogPin.
*/
double maker_getAverageMoistureValue(uint8_t analogPin, uint8_t numTests = 10);

#endif
